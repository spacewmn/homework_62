import React from 'react';
import History from "../../components/History/History";

const AboutUsPage = () => {
    return (
        <div>
            <div className="nav-bar">
                <div className="container">
                    <a href="/">Home</a>
                    <a href={"/about"}>About Us</a>
                    <a href={"production"}>Our production</a>
                    <a href={"/contacts"}>Contacts</a>
                </div>
            </div>
            <h1 className="container title">About Us</h1>
            <div className="about-us-block container">
                <History
                    img={'https://www.examen.ru/assets/images/2017/20171206_konditer_1.jpg'}
                />
                <History
                    img={'https://repost.uz/storage/uploads/file_5e4e11e9ec7354.202085121582174697.jpg'}
                />
                <History
                    img={'https://leonardo.osnova.io/87b1cee5-bd27-3265-f36c-7376de98bf3b/-/resize/800/-/progressive/yes/'}
                />
            </div>

            {/*<div className="history">*/}
            {/*    <img src="https://www.examen.ru/assets/images/2017/20171206_konditer_1.jpg" alt="Beginning" className="history-img"/>*/}
            {/*    <p className="history-txt">*/}
            {/*        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus dolore dolorem illum laudantium maxime obcaecati quas quisquam reprehenderit sint. Delectus esse impedit maiores ratione sequi suscipit voluptatibus voluptatum. Aliquid.*/}
            {/*    </p>*/}
            {/*</div>*/}
        </div>
    );
};

export default AboutUsPage;