import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import HomePage from "../HomePage/HomePage";
import AboutUsPage from "../AboutUsPage/AboutUsPage";
import Contacts from "../Contacts/Contacts";
import Production from "../Producion/Production";

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={HomePage}/>
          <Route path="/contacts" component={Contacts}/>
          <Route path="/production" component={Production}/>
          <Route path="/about" component={AboutUsPage}/>
        </Switch>
      </BrowserRouter>
    )
};

export default App;

