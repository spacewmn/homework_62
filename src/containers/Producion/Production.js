import React from 'react';
import Card from "../../components/Card/Card";
import './Production.css';

const Production = () => {
    return (
        <div>
            <div className="nav-bar">
                <div className="container">
                    <a href="/">Home</a>
                    <a href={"/about"}>About Us</a>
                    <a href={"production"}>Our production</a>
                    <a href={"/contacts"}>Contacts</a>
                </div>
            </div>
            <h1 className="title container">Our Production</h1>
            <div className="production-block container">
                <Card
                    img={'https://eda.yandex/images/1368744/cb691ff22f299c7c718fa86fe3f7af6f-400x400.jpeg'}
                    name={'Fruit-&-Choco Box'}
                    price={'600 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1380157/7ba5fbb67852cd2a6926b863675cbded-400x400.jpeg&quot'}
                    name={'Salt Caramel'}
                    price={'800 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1387779/69449cf94f5b2281366e266b12e5dd5a-400x400.jpeg&quot'}
                    name={'Cherryl Blossom'}
                    price={'300 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1387779/e6fcbac4d18fab2fec61f6e5a286f6b6-400x400.jpeg'}
                    name={'Caramel Brownie'}
                    price={'300 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1387779/d785f9148ef09b43d49516b8ab41ddab-400x400.jpeg&quot'}
                    name={'Cosmo Pie'}
                    price={'500 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1370147/4c186f8841a4f2dd5a746434b20dc2e0-400x400.JPG'}
                    name={'Assorti Box'}
                    price={'600 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1387779/603052127ebd5048c92a55bce90ca86e-400x400.jpeg&quot'}
                    name={'Dark-Coffee Brownie'}
                    price={'300 KGS'}
                />
                <Card
                    img={'https://eda.yandex/images/1368744/d12c58d800fc31e444e23005a88eb42f-400x400.jpeg&quot'}
                    name={'Nut-&-Coffee Box'}
                    price={'600 KGS'}
                />
                <Card
                    img={'https://cdn1.flamp.ru/d51f226ce4496e776333a94b3b35670f_600_600.jpg'}
                    name={'Milk-Choco Brownie'}
                    price={'300 KGS'}
                />

            </div>
        </div>
    );
};

export default Production;