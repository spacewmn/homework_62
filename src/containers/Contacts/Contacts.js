import React from 'react';
import './Contact.css';

const Contacts = () => {
    return (
        <div>
            <div className="nav-bar">
                <div className="container">
                    <a href="/">Home</a>
                    <a href={"/about"}>About Us</a>
                    <a href={"production"}>Our production</a>
                    <a href={"/contacts"}>Contacts</a>
                </div>
            </div>
            <h1 className="title container">Contacts</h1>
            <div className="contacts">
                <div className="container contact-block">
                    <div className="address contact">
                        <h3>Our address:</h3>
                        <p>Bishkek, Green str., 17</p>
                        <p>Bishkek, Chui str., 1</p>
                        <p>Bishkek, Baytik str., 256</p>
                        <p>Bishkek, Gagarin str., 99</p>
                        <p>Bishkek, Manas str., 36</p>
                        <p>Cholpon-Ata, Silver str., 7</p>
                        <p>Kara-Balta, Pobeda str., 22</p>
                    </div>
                    <div className="phone contact">
                        <h3>Our phones:</h3>
                        <p>0 312 00 55 00</p>
                        <p>0 555 55 00 55</p>
                        <p>0 700 77 00 77</p>
                        <p>0 777 00 77 00</p>
                    </div>
                    <div className="contact">
                        <h3>Follow us:</h3>
                        <a href="https://www.instagram.com/" className="link">
                            <img src="https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png" className="icon" alt=""/>
                        </a>
                        <a href="https://www.whatsapp.com/" className="link">
                            <img src="https://cdn.icon-icons.com/icons2/1826/PNG/512/4202050chatlogosocialsocialmediawhatsapp-115638_115663.png" className="icon" alt=""/>
                        </a>
                        <a href="https://www.facebook.com/" className="link">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAaVBMVEU6VZ////81Up0yT5xgc61+jbsvTZyXo8hFXaOirM0nSJmlrs7O1OUkRpkfQ5c1UZ1oerKHlcDp6/M9WKHHzeHV2ulxgravt9Td4Ozl6PGDkb7ByN5YbatQZ6j29/q3v9h2hrhMY6aQnMSsAnKHAAAC70lEQVR4nO3caXLiMBRFYdoihhhsQ5jDlPT+F9mdqv7bRrYQ7z7XOQug9BUWHiQzmRARERERERERERERERERqVe0IZQPCtaDHFwo62Yz/VrP3jtbrH0Sy7rYH46/YtpV1oPtX6jCbBel+2npThjq/Taa51AYmrdTH583YdHsP/r5nAnLTa/j05+w+ezv8yRsq/MQoB9hmPeegb6E5eU2DOhFWK4G+rwIw3CgD2G4Dge6ELaboXPQi7CJu4nwK6zvKUAHwvCVBHQgrFMmoQdh4jGqL2wviUB5YRP/uMKnsJ2mAtWF9XLkwjblcs2FsDqMXZh6LpQXhn06UFtYpZ7t5YX1wEczboTF/AlAaWFYP0OovPZU9pmGt9194W79sIq/Jt2uqqrytwbcxJ4NT9e6tR7soEIk8N749E2K7zjgubEe6dAi75yOboGxwrnTQ/RvRZTwIHy6e1SccF5Yj3N4UcJjbT3MhKKEh9J6mAlFCZWvyR4WJXxDqBxChPohRKgfQoT6IUSoH0KE+iFEqB9ChPqNQ1h0vKJcxWwt/aofvelsDFzNOlpECO9dH/DT+8ZUWMYgErNdfXuB8Ga7RPwCofHy2wuE59EfpQvbH9MXCNejF65st2q8QPhtu5Mhv/BmfE2TX/hhvGMqv3BpvBslv/BuvGUqv3A2+nloff+YX3gx3rmYX2j9CCC78Ga9NzO78GS9dTG7cDv679B8c2Z24e/R/9LsRy+8Wm/kzy60nobZhdb3TvmF9u8e5hYaP0p8gfB99PPQ/l2F3MKp+StDuYXWvuxC60eJ+YXm907ZhQJ/NlA+4d9ZOrrbH6WTzbyjmL+D/Oz6BNsl/H8V/y9E7TYpOz7BGveoceyn6QohQv0QItQPIUL9ECLUDyFC/RAi1A8hQv0QItQPIUL9ECLUDyFC/RAi1A8hQv0QItQPIUL9ECLUDyFC/RAi1A8hQv0QItQPIUL9ECLUDyFC/RAi1A8hQv0QItQPIUL9ECLs0R8aFUYEFLSeAgAAAABJRU5ErkJggg==" className="icon" alt=""/>
                        </a>
                        <a href="https://telegram.org/" className="link">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/600px-Telegram_logo.svg.png" className="icon" alt=""/>
                        </a>
                        <a href="https://www.youtube.com/" className="link">
                            <img src="https://cdn2.iconfinder.com/data/icons/social-icons-color/512/youtube-512.png" className="icon" alt=""/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Contacts;