import React from 'react';
import './HomePage.css'

const HomePage = () => {
    return (
        <div>
            <div className="nav-bar">
                <div className="container">
                    <a href="/">Home</a>
                    <a href={"/about"}>About Us</a>
                    <a href={"production"}>Our production</a>
                    <a href={"/contacts"}>Contacts</a>
                </div>
            </div>
            <h1 className="container title">Candy Mama</h1>
            <div className="home">
                <p className="home-txt">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti et laboriosam recusandae soluta vitae? A alias consequuntur debitis deserunt, dolore earum exercitationem explicabo facere fugiat, impedit ipsam laudantium minima necessitatibus nesciunt nisi nulla perspiciatis porro praesentium quidem quis ratione recusandae, soluta tempore unde vel vero voluptate voluptatem? Consequuntur, ea, ratione?
                </p>
            </div>
        </div>
    );
};

export default HomePage;