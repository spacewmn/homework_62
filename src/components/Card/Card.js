import React from 'react';
import './Card.css';

const Card = props => {
    return (
        <div className="card">
            <img src={props.img} className="card-img" alt="#"/>
            <div className="card-body">
                <h4 className="card-name">{props.name}</h4>
                <p className="card-price">{props.price}</p>
            </div>
        </div>
    );
};

export default Card;