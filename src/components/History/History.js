import React from 'react';
import './History.css';

const History = props => {
    return (
        <div className="history">
            <img src={props.img} alt="#" className="history-img"/>
            <p className="history-txt">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus dolore dolorem illum laudantium maxime obcaecati quas quisquam reprehenderit sint. Delectus esse impedit maiores ratione sequi suscipit voluptatibus voluptatum. Aliquid.
            </p>
        </div>
    );
};

export default History;